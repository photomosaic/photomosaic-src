/*
 * This file is part of photomosaic.
 *
 * Copyright 2013 Thomas Fischer <fischer@unix-ag.uni-kl.de>
 *
 * photomosaic is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * photomosaic is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with photomosaic.  If not, see <http://www.gnu.org/licenses/>.
 */


#include<sstream>

#include "general.h"

bool endsWith(const std::string &fullString, const std::string &ending)
{
    if (fullString.length() >= ending.length()) {
        return (0 == fullString.compare(fullString.length() - ending.length(), ending.length(), ending));
    } else {
        return false;
    }
}

int gcd(unsigned int a, unsigned int b)
{
    int c;
    while (b) {
        c = a;
        a = b;
        b = c % b;
    }
    return a;
}

int toInt(const std::string &string)
{
    int result;
    std::stringstream stream(string);
    stream >> result;
    return result;
}
