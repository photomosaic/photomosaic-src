/*
 * This file is part of photomosaic.
 *
 * Copyright 2013 Thomas Fischer <fischer@unix-ag.uni-kl.de>
 *
 * photomosaic is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * photomosaic is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with photomosaic.  If not, see <http://www.gnu.org/licenses/>.
 */


#include <fstream>

#include <sys/types.h>
#include <dirent.h>

#include "dlib/gui_widgets.h"
#include "dlib/image_io.h"
#include "dlib/image_transforms.h"
#include "dlib/pixel.h"
#include "dlib/image_loader/png_loader.h"
#include "dlib/optimization.h"
#include "dlib/matrix/matrix_utilities.h"

#include "image_io.h"
#include "image_op.h"
#include "general.h"

using namespace std;
using namespace dlib;

struct ArgOptions {
    std::vector< std::string > tileDirectories;
    std::string baseImageFilename;
    std::string outputFilename;
    unsigned int numTilesPerAxis;
    unsigned int scaleDownTilesFactor;
    unsigned int sublevel;
};

struct Options {
    array2d<rgb_pixel> baseImage;
    unsigned int numTilesPerAxis;
    unsigned int scaleDownTilesFactor;
    ImageIO imgIO;
    std::string outputFilename;
    unsigned int sublevel;
};

template <
typename in_image_type,
         typename out_image_type
         >
void scale_down(
    const in_image_type &in_img,
    out_image_type &out_img,
    int divisor = 2
)
{
    DLIB_ASSERT(is_same_object(in_img, out_img) == false
                && divisor >= 1
                && in_img.nr() >= divisor
                && in_img.nc() >= divisor,
                "\t void scale_down()"
                << "\n\t is_same_object(in_img,out_img): " << is_same_object(in_img, out_img)
                << "\n\t divisor >= 1: " << divisor >= 1
                << "\n\t in_img.nr() >= divisor: " << in_img.nr() >= divisor
                << "\n\t in_img.nc() >= divisor: " << in_img.nc() >= divisor
               );

    const double gamma = 2.2;
    const double inv_gamma = 1.0 / gamma;
    const int sq_divisor = divisor * divisor;

    out_img.set_size(in_img.nr() / divisor, in_img.nc() / divisor);

    for (int oy = out_img.nr() - 1, iy = oy * divisor; oy >= 0; --oy, iy -= divisor) {
        for (int ox = out_img.nc() - 1, ix = ox * divisor; ox >= 0; --ox, ix -= divisor) {
            double red = 0.0, green = 0.0, blue = 0.0;
            for (int by = divisor - 1; by >= 0; --by) {
                for (int bx = divisor - 1; bx >= 0; --bx) {
                    red += pow(in_img[iy + by][ix + bx].red, gamma);
                    green += pow(in_img[iy + by][ix + bx].green, gamma);
                    blue += pow(in_img[iy + by][ix + bx].blue, gamma);
                }
            }
            red /= sq_divisor;
            green /= sq_divisor;
            blue /= sq_divisor;
            out_img[oy][ox].red = (int)pow(red, inv_gamma);
            out_img[oy][ox].green = (int)pow(green, inv_gamma);
            out_img[oy][ox].blue = (int)pow(blue, inv_gamma);
        }
    }
}

void pos2xy(const int pos, const int numtilesx, int &x, int &y)
{
    DLIB_ASSERT(pos >= 0
                && numtilesx >= 0,
                "\t void pos2xy()"
                << "\n\t pos>=0: " << pos >= 0
                << "\n\t numtilesx>=0: " << numtilesx >= 0
               );
    y = pos / numtilesx;
    x = pos % numtilesx;
}

bool parse_arguments(const int argc, char **argv, ArgOptions &argOptions)
{
    static const std::string optionN("-n");
    static const std::string optionS("-s");
    static const std::string optionI("-i");
    static const std::string optionO("-o");
    static const std::string optionSublevel("-S");
    static const std::string optionHelpShort("-h");
    static const std::string optionHelpLong("--help");

    argOptions.tileDirectories.clear();
    argOptions.numTilesPerAxis = 0;
    argOptions.scaleDownTilesFactor = 0;
    argOptions.baseImageFilename = std::string();
    argOptions.outputFilename = std::string();
    argOptions.sublevel = 1;

    for (int i = 1; i < argc; ++i) {
        const std::string arg(argv[i]);
        if (i < argc - 1 && arg.compare(optionN) == 0) {
            argOptions.numTilesPerAxis = toInt(std::string(argv[i + 1]));
            ++i;
        } else if (i < argc - 1 && arg.compare(optionS) == 0) {
            argOptions.scaleDownTilesFactor = toInt(std::string(argv[i + 1]));
            ++i;
        } else if (i < argc - 1 && arg.compare(optionI) == 0) {
            argOptions.baseImageFilename = std::string(argv[i + 1]);
            ++i;
        } else if (i < argc - 1 && arg.compare(optionO) == 0) {
            argOptions.outputFilename = std::string(argv[i + 1]);
            ++i;
        } else if (arg.compare(optionSublevel) == 0) {
            argOptions.sublevel = 2;
        } else if (optionHelpShort.compare(arg) == 0 || optionHelpLong.compare(arg) == 0) {
            std::cout << "PhotoMosaic program by Thomas Fischer" << std::endl;
            std::cout << "Released under the GPL 3" << std::endl;
            std::cout << "https://gitorious.org/photomosaic" << std::endl;
            std::cout << std::endl;
            std::cout << "Usage: photomosaic -n A -s B [-S] -i C -o D E F G ..." << std::endl;
            std::cout << "where" << std::endl;
            std::cout << "  -n A     number of tiles per axis, mandatory" << std::endl;
            std::cout << "  -s B     factor to scale down tiles, mandatory" << std::endl;
            std::cout << "  -S       generate sublevel images, optional, default is " << (argOptions.sublevel == 1 ? "off" : "on") << std::endl;
            std::cout << "  -i C     base image's filename, mandatory" << std::endl;
            std::cout << "  -o D     filename for result, mandatory" << std::endl;
            std::cout << "  E,F,...  a list of directories to scan for tiles" << std::endl;
            return false;
        } else if (argv[i][0] != '-')
            argOptions.tileDirectories.push_back(arg);
        else {
            std::cerr << "Cannot guess what this means: " << argv[i] << std::endl;
            return false;
        }
    }

    if (argOptions.tileDirectories.empty()) {
        std::cerr << "No directories with tiles specified" << std::endl;
        return false;
    }
    if (argOptions.numTilesPerAxis <= 1) {
        std::cerr << "Tiles-per-axis is invalid: " << argOptions.numTilesPerAxis << std::endl;
        return false;
    }
    if (argOptions.scaleDownTilesFactor <= 1) {
        std::cerr << "scale-down-tiles-factor is invalid: " << argOptions.scaleDownTilesFactor << std::endl;
        return false;
    }
    if (argOptions.baseImageFilename.empty()) {
        std::cerr << "No base image filename specified" << std::endl;
        return false;
    }
    if (argOptions.outputFilename.empty()) {
        std::cerr << "No output filename specified" << std::endl;
        return false;
    }

    return true;
}

bool process_arguments(const ArgOptions &argOptions, Options &options)
{
    options.numTilesPerAxis = argOptions.numTilesPerAxis;
    options.scaleDownTilesFactor = argOptions.scaleDownTilesFactor;
    options.outputFilename = argOptions.outputFilename;
    options.sublevel = argOptions.sublevel;

    std::cout << "Loading " << argOptions.baseImageFilename << std::endl;
    load_image(options.baseImage, argOptions.baseImageFilename);

    if (options.baseImage.nc() % options.numTilesPerAxis != 0) {
        std::cerr << "Base image width " << options.baseImage.nc() << " cannot be divided by " << options.numTilesPerAxis << std::endl;
        return false;
    }
    if (options.baseImage.nr() % options.numTilesPerAxis != 0) {
        std::cerr << "Base image height " << options.baseImage.nr() << " cannot be divided by " << options.numTilesPerAxis << std::endl;
        return false;
    }

    for (std::vector< std::string >::const_iterator it = argOptions.tileDirectories.begin(); it != argOptions.tileDirectories.end(); ++it)
        if (!options.imgIO.scanDirectory(*it, options.sublevel))
            return false;

    if (options.imgIO.numImages() < options.numTilesPerAxis * options.numTilesPerAxis) {
        std::cerr << "Too few images known for the requested number of tiles" << std::endl;
        return false;
    }

    for (int i = options.imgIO.numImages() - 1; i >= 0; --i) {
        int width, height;
        options.imgIO.size(i, width, height);
        width *= 1 << (options.imgIO.sublevel(i) - 1);
        height *= 1 << (options.imgIO.sublevel(i) - 1);
        if (width % options.scaleDownTilesFactor != 0) {
            std::cerr << "Tile image width " << width << " cannot be divided by " << options.scaleDownTilesFactor << std::endl;
            return false;
        }
        if (height % options.scaleDownTilesFactor != 0) {
            std::cerr << "Tile image height " << height << " cannot be divided by " << options.scaleDownTilesFactor << std::endl;
            return false;
        }
    }

    return true;
}

int main(int argc, char **argv)
{
    ArgOptions argOptions;
    Options options;

    if (!parse_arguments(argc, argv, argOptions))
        return 1;
    if (!process_arguments(argOptions, options))
        return 1;

    ::srand(time(NULL));

    try {
        const int numavailtildes = options.imgIO.numImages();
        const int mod = std::max(23, numavailtildes / 50);
        std::cout << "Know " << numavailtildes << " tiles" << std::endl;

        const int numtiles = options.numTilesPerAxis * options.numTilesPerAxis;

        std::cout << "Computing distances " << std::flush;
        const int fpr_w = options.baseImage.nc() / options.numTilesPerAxis;
        const int fpr_h = options.baseImage.nr() / options.numTilesPerAxis;
        array2d<int> distance_map;
        distance_map.set_size(numavailtildes, numtiles);
        for (int y = 0, maxprogress = options.baseImage.nr() * options.baseImage.nc() / fpr_w / fpr_h, progressmod = max(5, maxprogress / 50), pos = 0; y + fpr_h <= options.baseImage.nr(); y += fpr_h) {
            for (int x = 0; x + fpr_w <= options.baseImage.nc(); x += fpr_w, ++pos) {
                const std::string fpr = ImageOp::fingerprint(options.baseImage, x, y, fpr_w, fpr_h);
                int mind = 0x0fffffff, mintile = -1;
                if (pos % progressmod == progressmod / 2)
                    std::cout << "." << std::flush;
                for (int tile = numavailtildes - 1; tile >= 0; --tile) {
                    const int d = ImageOp::fingerprint_distance(fpr, options.imgIO.fingerprint(tile));
                    distance_map[tile][pos] = d;
                }
            }
        }
        std::cout << std::endl;

        std::cout << "Creating cost matrix " << std::flush;
        const long matrix_size = numtiles > numavailtildes ? numtiles : numavailtildes;
        matrix<long> costmatrix = ones_matrix<long>(matrix_size, matrix_size);
        for (int tile = numavailtildes - 1; tile >= 0; --tile) {
            for (int pos = numtiles - 1; pos >= 0; --pos) {
                costmatrix(tile, pos) = 0x0fffffff - distance_map[tile][pos];
            }
            if (tile % mod == mod / 2)
                std::cout << "." << std::flush;
            for (int pos = numtiles; pos < matrix_size; ++pos) {
                costmatrix(tile, pos) = 0;
            }
        }
        for (int tile = numavailtildes; tile < matrix_size; ++tile) {
            for (int pos = numtiles; pos < matrix_size; ++pos) {
                costmatrix(tile, pos) = 0;
            }
        }
        std::cout << std::endl;

        std::cout << "Finding best cost assignment" << std::flush;
        std::vector<long> best = max_cost_assignment(costmatrix);
        std::cout << std::endl;

        std::cout << "Creating image " << std::flush;
        const int tile_w = options.baseImage.nc() / options.scaleDownTilesFactor;
        const int tile_h = options.baseImage.nr() / options.scaleDownTilesFactor;
        array2d<rgb_pixel> out_img;
        out_img.set_size(options.numTilesPerAxis * tile_h, options.numTilesPerAxis * tile_w);
        for (int tile = numavailtildes - 1; tile >= 0; --tile) {
            int x, y;
            pos2xy(best[tile], options.numTilesPerAxis, x, y);

            if (tile % mod == mod / 2)
                std::cout << "." << std::flush;

            if (x >= 0 && y >= 0 && x < options.numTilesPerAxis && y < options.numTilesPerAxis) {
                x *= tile_w;
                y *= tile_h;

                const std::string &matchFilename = options.imgIO.filename(tile);
                const int sublevel = options.imgIO.sublevel(tile);
                const int subposx = options.imgIO.subposx(tile);
                const int subposy = options.imgIO.subposy(tile);

                array2d<rgb_pixel> buffer_img;
                load_image(buffer_img, matchFilename);

                if (options.scaleDownTilesFactor == 1 && sublevel == 1)
                    ImageOp::copy_image_to_image(out_img, x, y, buffer_img, 0, 0);
                else {
                    array2d<rgb_pixel> buffer2_img;
                    scale_down(buffer_img, buffer2_img, options.scaleDownTilesFactor / sublevel);
                    const int tx = tile_w * subposx / 2;
                    const int ty = tile_h * subposy / 2;
                    ImageOp::copy_image_to_image(out_img, x, y, buffer2_img, tx, ty, tile_w, tile_h);
                }
            }
        }
        std::cout << std::endl;

        std::cout << "Saving mosaic image to " << options.outputFilename << std::endl;
        save_png(out_img, options.outputFilename);

    } catch (exception &e) {
        cout << "exception thrown: " << e.what() << endl;
    }
}

