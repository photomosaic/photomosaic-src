/*
 * This file is part of photomosaic.
 *
 * Copyright 2013 Thomas Fischer <fischer@unix-ag.uni-kl.de>
 *
 * photomosaic is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * photomosaic is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with photomosaic.  If not, see <http://www.gnu.org/licenses/>.
 */


#ifndef GENERAL_H
#define GENERAL_H

#include <iostream>

bool endsWith(const std::string &fullString, const std::string &ending);

int gcd(unsigned int a, unsigned int b);

int toInt(const std::string &string);

#endif // GENERAL_H
