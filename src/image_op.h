/*
 * This file is part of photomosaic.
 *
 * Copyright 2013 Thomas Fischer <fischer@unix-ag.uni-kl.de>
 *
 * photomosaic is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * photomosaic is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with photomosaic.  If not, see <http://www.gnu.org/licenses/>.
 */


#ifndef IMAGE_OP_H
#define IMAGE_OP_H

#include "dlib/array2d.h"
#include "dlib/pixel.h"

class ImageOp
{
public:
    static const char *fingerprintmap;

    ImageOp();

    static void scale_down(
        const dlib::array2d<dlib::rgb_pixel> &in_img,
        dlib::array2d<dlib::rgb_pixel> &out_img,
        int divisor = 2
    );

    static void scale_down(
        const dlib::array2d<dlib::rgb_pixel> &in_img,
        dlib::array2d<dlib::rgb_pixel> &out_img,
        int divisorx, int divisory
    );

    static void scale_down(
        const dlib::array2d<dlib::rgb_pixel> &in_img,
        const int x, const int y, const int w, const int h,
        dlib::array2d<dlib::rgb_pixel> &out_img,
        int divisorx, int divisory
    );


    static void copy_image_to_image(dlib::array2d<dlib::rgb_pixel> &dest_img, int dest_offset_x, int dest_offset_y, const dlib::array2d<dlib::rgb_pixel> &src_img, int src_offset_x, int src_offset_y, int width = 0x0fffffff, int height = 0x0fffffff);

    static const std::string fingerprint(const dlib::array2d<dlib::rgb_pixel> &image);
    static const std::string fingerprint(const dlib::array2d<dlib::rgb_pixel> &image, const int x, const int y, const int w, const int h);
    static int fingerprint_distance(const std::string &fpr1, const std::string &fpr2);
};

#endif // IMAGE_OP_H
