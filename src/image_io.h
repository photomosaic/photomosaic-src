/*
 * This file is part of photomosaic.
 *
 * Copyright 2013 Thomas Fischer <fischer@unix-ag.uni-kl.de>
 *
 * photomosaic is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * photomosaic is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with photomosaic.  If not, see <http://www.gnu.org/licenses/>.
 */


#ifndef IMAGE_IO_H
#define IMAGE_IO_H

#include "dlib/array.h"
#include "dlib/array2d.h"
#include "dlib/map.h"

class ImageIO
{
public:
    struct Tile {
        std::string filename;
        int width, height;
        int sublevel, subposx, subposy;
        std::string fingerprint;

        Tile(const std::string &fname, const int w, const int h, const std::string &fpr, const int sl = 1, const int spx = 0, const int spy = 0)
            : filename(fname), width(w), height(h), fingerprint(fpr), sublevel(sl), subposx(spx), subposy(spy) {
            /// nothing
        }
    };

    ImageIO();

    bool scanDirectory(const std::string &directory_name, unsigned int maxSublevel = 2);

    int numImages() const;

    const std::string &closestFit(const std::string &original) const;

    const std::string &filename(const int index) const;
    const std::string &fingerprint(const int index) const;

    void size(const int index, int &width, int &height) const;

    const int sublevel(const int index) const;
    const int subposx(const int index) const;
    const int subposy(const int index) const;
    const std::string &filename(const std::string &fpr) const;


private:
    std::vector< struct Tile > m_fingerprints;

    bool loadFingerprints(const std::string &directory_name);
    bool saveFingerprints(const std::string &directory_name) const;
};

#endif // IMAGE_IO_H
