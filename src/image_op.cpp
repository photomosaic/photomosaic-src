/*
 * This file is part of photomosaic.
 *
 * Copyright 2013 Thomas Fischer <fischer@unix-ag.uni-kl.de>
 *
 * photomosaic is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * photomosaic is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with photomosaic.  If not, see <http://www.gnu.org/licenses/>.
 */


#include "image_op.h"

using namespace dlib;

const char *ImageOp::fingerprintmap = "#$%&'()*+,-./0123456789:;<=>?@ABCDEFGHIJKLMOPQRSTUVWXYZ[\\]^_`abcefghijklmnopqrstuvwxyz";

ImageOp::ImageOp()
{
    /// nothing
}

const std::string ImageOp::fingerprint(const dlib::array2d<dlib::rgb_pixel> &image)
{
    return fingerprint(image, 0, 0, image.nc(), image.nr());
}

const std::string ImageOp::fingerprint(const dlib::array2d<dlib::rgb_pixel> &image, const int x, const int y, const int w, const int h)
{
    const int fprwidth = 6, fprheight = 6;
    const int divisorx = w / fprwidth;
    const int divisory = h / fprheight;

    array2d<rgb_pixel> thumbnail;
    scale_down(image, x, y, w, h, thumbnail, divisorx, divisory);

    char fpr_array[fprwidth * fprheight * 3 + 2];
    int p = 0;
    for (int y = 0; y < fprwidth; ++y)
        for (int x = 0; x < fprheight; ++x) {
            fpr_array[p++] = fingerprintmap[thumbnail[y][x].red / 4];
            fpr_array[p++] = fingerprintmap[thumbnail[y][x].green / 4];
            fpr_array[p++] = fingerprintmap[thumbnail[y][x].blue / 4];
        }
    fpr_array[p++] = '\0';

    return std::string(fpr_array);
}

int ImageOp::fingerprint_distance(const std::string &fpr1, const std::string &fpr2)
{
    int distance = 0;
    for (int i = fpr1.length() - 1; i >= 0; --i) {
        const int delta = (int)fpr1[i] - (int)fpr2[i];
        distance += delta * delta;
    }
    return distance;
}

void ImageOp::scale_down(
    const array2d<rgb_pixel> &in_img,
    array2d<rgb_pixel> &out_img,
    int divisor
)
{
    return scale_down(in_img, out_img, divisor, divisor);
}

void ImageOp::scale_down(
    const array2d<rgb_pixel> &in_img,
    array2d<rgb_pixel> &out_img,
    int divisorx, int divisory
)
{
    return scale_down(in_img, 0, 0, in_img.nc(), in_img.nr(), out_img, divisorx, divisory);
}

void ImageOp::scale_down(
    const array2d<rgb_pixel> &in_img,
    const int x, const int y, const int w, const int h,
    array2d<rgb_pixel> &out_img,
    int divisorx, int divisory
)
{
    DLIB_ASSERT(is_same_object(in_img, out_img) == false
                && divisorx >= 1
                && divisory >= 1
                && x + w < in_img.nc();
                && y + h < in_img.nr();
                && h >= divisory
                && w >= divisorx,
                "\t void scale_down()"
                << "\n\t is_same_object(in_img,out_img): " << is_same_object(in_img, out_img)
                << "\n\t divisorx >= 1: " << divisorx >= 1
                << "\n\t divisory >= 1: " << divisory >= 1
                << "\n\t x+w < in_img.nc(): " << x + w < in_img.nc()
                << "\n\t y+h < in_img.nr(): " << y + h < in_img.nr()
                << "\n\t h >= divisory: " << h >= divisory
                << "\n\t w >= divisorx: " << w >= divisorx
               );

    const double gamma = 2.2;
    const double inv_gamma = 1.0 / gamma;
    const int sq_divisor = divisorx * divisory;

    out_img.set_size(h / divisory, w / divisorx);
    const int shiftx = (w % divisorx) / 2;
    const int shifty = (h % divisory) / 2;

    for (int oy = out_img.nr() - 1, iy = y + oy * divisory; oy >= 0; --oy, iy -= divisory) {
        for (int ox = out_img.nc() - 1, ix = x + ox * divisorx; ox >= 0; --ox, ix -= divisorx) {
            double red = 0.0, green = 0.0, blue = 0.0;
            for (int by = divisory - 1; by >= 0; --by) {
                for (int bx = divisorx - 1; bx >= 0; --bx) {
                    red += pow(in_img[iy + by + shifty][ix + bx + shiftx].red, gamma);
                    green += pow(in_img[iy + by + shifty][ix + bx + shiftx].green, gamma);
                    blue += pow(in_img[iy + by + shifty][ix + bx + shiftx].blue, gamma);
                }
            }
            red /= sq_divisor;
            green /= sq_divisor;
            blue /= sq_divisor;
            out_img[oy][ox].red = (int)pow(red, inv_gamma);
            out_img[oy][ox].green = (int)pow(green, inv_gamma);
            out_img[oy][ox].blue = (int)pow(blue, inv_gamma);
        }
    }
}

void ImageOp::copy_image_to_image(array2d<rgb_pixel> &dest_img, int dest_offset_x, int dest_offset_y, const array2d<rgb_pixel> &src_img, int src_offset_x, int src_offset_y, int width, int height)
{
    DLIB_ASSERT(is_same_object(dest_img, src_img) == false
                && dest_offset_x >= 0 && dest_offset_y >= 0 && dest_offset_x + src_img.nc() <= dest_img.nc() && dest_offset_y + src_img.nr() <= dest_img.nr(),
                "\t void copy_image_to_image()"
                << "\n\t is_same_object(big_img, small_img): " << is_same_object(dest_img, src_img)
                << "\n\t big_x>=0: " << dest_offset_x >= 0
                << "\n\t big_y>=0: " << dest_offset_y >= 0
                << "\n\t big_x+small_img.nc()<=big_img.nc(): " << (dest_offset_x + src_img.nc() <= dest_img.nc())
                << "\n\t big_y+small_img.nr()<=big_img.nr(): " << (dest_offset_y + src_img.nr() <= dest_img.nr())
               );
    const int w = std::min<const long int>(width, std::min<const long int>(src_img.nc() - src_offset_x, dest_img.nc() - dest_offset_x));
    const int h = std::min<const long int>(height, std::min<const long int>(src_img.nr() - src_offset_y, dest_img.nr() - dest_offset_y));

    for (int y = h - 1; y >= 0; --y) {
        const int srcy = y + src_offset_y;
        const int desty = y + dest_offset_y;
        for (int x = w - 1; x >= 0; --x) {
            const int srcx = x + src_offset_x;
            const int destx = x + dest_offset_x;
            dest_img[desty][destx] = src_img[srcy][srcx];
        }
    }
}
