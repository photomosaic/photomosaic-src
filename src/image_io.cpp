/*
 * This file is part of photomosaic.
 *
 * Copyright 2013 Thomas Fischer <fischer@unix-ag.uni-kl.de>
 *
 * photomosaic is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * photomosaic is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with photomosaic.  If not, see <http://www.gnu.org/licenses/>.
 */


#include <iostream>
#include <fstream>
#include <sstream>
#include <vector>

#include "dlib/dir_nav.h"
#include "dlib/pixel.h"
#include "dlib/image_io.h"
#include "dlib/hash_map.h"

#include "image_op.h"
#include "image_io.h"
#include "general.h"

using namespace dlib;

ImageIO::ImageIO()
{
    /// nothing
}

bool ImageIO::loadFingerprints(const std::string &directory_name)
{
    const std::string full_directory_name = directory(directory_name).full_name();
    std::string fpr_name = full_directory_name;
    fpr_name = fpr_name.append("/fingerprints.txt");
    std::ifstream file(fpr_name.data(), std::ifstream::in);

    if (file.good()) {
        const size_t previouslyLoadedFingerprints = m_fingerprints.size();
        std::string line;
        while (file.good() && std::getline(file, line)) {
            std::string filename = full_directory_name;
            if (filename[filename.length() - 1] != '/')
                filename = filename.append("/");
            filename = filename.append(line);
            if (file.good() && std::getline(file, line)) {
                if (line.compare(0, 5, "size ") == 0) {
                    std::istringstream iss1(line.substr(5), std::istringstream::in);
                    int width, height;
                    iss1 >> width >> height;
                    if (file.good() && std::getline(file, line)) {
                        if (line.compare(0, 9, "sublevel ") == 0) {
                            std::istringstream iss2(line.substr(9), std::istringstream::in);
                            int sublevel, subposx, subposy;
                            iss2 >> sublevel >> subposx >> subposy;
                            if (file.good() && std::getline(file, line))
                                m_fingerprints.push_back(Tile(filename, width, height, line, sublevel, subposx, subposy));
                        } else
                            m_fingerprints.push_back(Tile(filename, width, height, line));
                    }
                }
            }
        }

        std::cout << "Loaded " << (m_fingerprints.size() - previouslyLoadedFingerprints) << " fingerprints from directory " << full_directory_name << std::endl;
    } else {
        std::cerr << "No fingerprint file found in directory " << full_directory_name << std::endl;
        file.close();
        return false;
    }

    file.close();
    return true;
}

bool ImageIO::saveFingerprints(const std::string &directory_name) const
{
    const std::string full_directory_name = directory(directory_name).full_name();
    std::string fpr_name = full_directory_name;
    fpr_name = fpr_name.append("/fingerprints.txt");

    std::cout << "Saving fingerprints to " << fpr_name << std::endl;

    try {
        std::ofstream file(fpr_name.data(), std::ofstream::out);

        const size_t directory_name_len = full_directory_name.length();
        int count = 0;
        for (int i = 0; i < m_fingerprints.size(); ++i) {
            const std::string line = m_fingerprints[i].filename;
            int p = line.rfind('/');
            if (directory_name_len == p && line.substr(0, p).compare(full_directory_name.substr(0, p)) == 0) {
                file << line.substr(p + 1) << std::endl;
                file << "size " << m_fingerprints[i].width << ' ' << m_fingerprints[i].height << std::endl;
                file << "sublevel " << m_fingerprints[i].sublevel << ' ' << m_fingerprints[i].subposx << ' ' << m_fingerprints[i].subposy << std::endl;
                file << m_fingerprints[i].fingerprint << std::endl;
                ++count;
            }
        }

        file.close();

        std::cout << "Saved " << count << " fingerprints" << std::endl;
    } catch (directory::dir_not_found &e) {
        std::cerr << "dir " << full_directory_name << " not found or accessible: " << e.info << std::endl;
        return false;
    }

    return true;
}

bool ImageIO::scanDirectory(const std::string &directory_name, unsigned int maxSublevel)
{
    try {
        std::cout << "Scanning directory " << directory_name << std::endl;
        directory dir(directory_name);
        std::vector<file> files;
        dir.get_files(files);
        std::cout << "Found " << files.size() << " files" << std::endl;

        loadFingerprints(directory_name);

        for (long i = 0; i < files.size(); ++i) {
            if (!endsWith(files[i].name(), std::string(".png")) && !endsWith(files[i].name(), std::string(".jpg")))
                continue;

            bool knownFile = false;
            std::string compareTo("/");
            compareTo = compareTo.append(files[i].name());
            for (int f = m_fingerprints.size() - 1; !knownFile && f >= 0; --f) {
                const std::string &line = m_fingerprints[f].filename;
                knownFile = endsWith(line, compareTo);
            }
            if (knownFile)
                continue;

            std::cout << "Loading (" << (i + 1) << "/" << files.size() << "): " << files[i].name() << std::endl;

            /// Load, analyze, and memorize original picture
            array2d<rgb_pixel> img;
            load_image(img, files[i].full_name());
            m_fingerprints.push_back(Tile(files[i].full_name(), img.nc(), img.nr(), ImageOp::fingerprint(img)));

            /// Sublevel 2 on base image
            array2d<rgb_pixel> subimg;
            for (int sublevel = 2; sublevel <= maxSublevel; ++sublevel) {
                std::cout << "Rendering sublevel " << sublevel << std::endl;
                const int w = img.nc() >> (sublevel - 1);
                const int h = img.nr() >> (sublevel - 1);
                const int xhalf = w / 2;
                const int yhalf = h / 2;

                for (int xstart = 0; xstart < w; xstart += xhalf) {
                    for (int ystart = 0; ystart < h; ystart += yhalf) {
                        for (int x = xstart; x + w <= img.nc(); x += w) {
                            for (int y = ystart; y + h <= img.nr(); y += h) {
                                int spx = x / xhalf;
                                int spy = y / yhalf;
                                m_fingerprints.push_back(Tile(files[i].full_name(), w, h, ImageOp::fingerprint(img, x, y, w, h), sublevel, spx, spy));
                            }
                        }
                    }
                }
            }
        }

        if (!saveFingerprints(directory_name))
            return false;

    } catch (file::file_not_found &e) {
        std::cerr << "file not found or accessible: " << e.info << std::endl;
        return false;
    } catch (directory::dir_not_found &e) {
        std::cerr << "dir not found or accessible: " << e.info << std::endl;
        return false;
    } catch (directory::listing_error &e) {
        std::cerr << "listing error: " << e.info << std::endl;
        return false;
    }

    return true;
}

const std::string &ImageIO::closestFit(const std::string &original) const
{
    int besti = -1, mindistance = 0x0fffffff;

    for (int i = m_fingerprints.size() - 1; i >= 0; --i) {
        const int d = ImageOp::fingerprint_distance(original, m_fingerprints[i].fingerprint);
        if (d < mindistance) {
            mindistance = d;
            besti = i;
        }
    }
    return m_fingerprints[besti].fingerprint;
}

const std::string &ImageIO::filename(const int index) const
{
    return m_fingerprints[index].filename;
}

const std::string &ImageIO::fingerprint(const int index) const
{
    return m_fingerprints[index].fingerprint;
}

void ImageIO::size(const int index, int &width, int &height) const
{
    width = m_fingerprints[index].width;
    height = m_fingerprints[index].height;
}

const int ImageIO::sublevel(const int index) const
{
    return m_fingerprints[index].sublevel;
}

const int ImageIO::subposx(const int index) const
{
    return m_fingerprints[index].subposx;
}

const int ImageIO::subposy(const int index) const
{
    return m_fingerprints[index].subposy;
}

const std::string &ImageIO::filename(const std::string &fpr) const
{
    for (int i = m_fingerprints.size() - 1; i >= 0; --i)
        if (m_fingerprints[i].fingerprint.compare(fpr) == 0)
            return m_fingerprints[i].filename;
    return m_fingerprints[0].filename;
}

int ImageIO::numImages() const
{
    return m_fingerprints.size();
}
